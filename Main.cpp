#include <iostream>

void even_OddNumbersFrom0To10(int n, bool even_odd)
{
    for ( int x = even_odd; x <= n; x += 2)
    {
        std::cout << x << '\n';
    }
}

int main()
{
    even_OddNumbersFrom0To10(10, false);
}
